const Button = ({
  label,
  iconURL,
  backgroundColor,
  borderColor,
  textColor,
}) => {
  return (
    <button
      className={`flex justify-center items-center gap-2 px-7 py-4 border font-montserrat text-lg leading-none rounded-full
        ${
          backgroundColor
            ? `${backgroundColor} ${borderColor} ${textColor}`
            : "bg-coral-red text-white border-coral-red"
        } `}
    >
      {label}
      {iconURL && (
        <img
          src={iconURL}
          alt="arrow right icon"
          className="w-6 h-6 ml-2 rounded-full"
        />
      )}
    </button>
  );
};

export default Button;
